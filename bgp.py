import socket
import time
from OpenSSL import crypto
import hashlib

# BGP message types
OPEN = 1
UPDATE = 2
NOTIFICATION = 3
KEEPALIVE = 4

# BGP speaker class
class BGPSpeaker:
    def __init__(self, local_ip, local_as, remote_ip, remote_as, listening_port, conn_port):
        self.local_ip = local_ip
        self.local_as = local_as
        self.remote_ip = remote_ip
        self.remote_as = remote_as
        self.listening_port = listening_port
        self.conn_port = conn_port
        self.socket = None
        self.finished = False
        self.connected = False
        self.cert_file = f"{self.local_as}_certificate.pem"
        self.opp_cert_file = f"{self.remote_as}_certificate.pem"
        self.private_key_file = f"{self.local_as}_private_key.pem"
    
    def generate_certificate(self):
        # Generate a new private key
        pkey = crypto.PKey()
        pkey.generate_key(crypto.TYPE_RSA, 2048)

        # Create a self-signed certificate
        cert = crypto.X509()
        cert.get_subject().CN = f"AS{self.local_as}"
        cert.set_serial_number(1000)
        cert.gmtime_adj_notBefore(0)
        cert.gmtime_adj_notAfter(10 * 365 * 24 * 60 * 60)  # Expires in 10 years
        cert.set_issuer(cert.get_subject())
        cert.set_pubkey(pkey)
        cert.sign(pkey, "sha256")

        # Save the certificate and private key to files
        with open(self.cert_file, "wb") as f:
            f.write(crypto.dump_certificate(crypto.FILETYPE_PEM, cert))
        with open(self.private_key_file, "wb") as f:
            f.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, pkey))
    
    def load_opp_certificate(self):
        # Load the certificate from file
        with open(self.opp_cert_file, "rb") as f:
            cert_data = f.read()
        return crypto.load_certificate(crypto.FILETYPE_PEM, cert_data)

    def load_private_key(self):
        # Load the private key from file
        with open(self.private_key_file, "rb") as f:
            key_data = f.read()
        return crypto.load_privatekey(crypto.FILETYPE_PEM, key_data)
    
    def verify_open_message(self, open_message):
        # Extract the OPEN message and the signature
        signature = open_message[-256:]
        open_message = open_message[:-256]
      
        # Load the opponent's certificate
        cert = self.load_opp_certificate()

        # Calculate the hash of the OPEN message
        digest = hashlib.sha256(open_message).digest()

        # Verify the signature using the opponent's public key
        try:
            crypto.verify(cert, signature, digest, "sha256")
            print("Signature verified successfully.")
            return True
        except crypto.Error:
            print("Signature verification failed.")
            return False

    def connect(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.remote_ip, self.conn_port))
        print("TCP Connection Established")

    def listen(self):
        listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        listen_socket.bind((self.local_ip, self.listening_port))
        listen_socket.listen(1)
        self.socket, _ = listen_socket.accept()
        print("TCP Connection Established")

    def send_message(self, message_type, message, need_response=True):
        # Construct BGP message header
        marker = b'\xff' * 16
        message_length = len(message) + 20
        header = marker + message_length.to_bytes(2, 'big') + message_type.to_bytes(1, 'big') + int(need_response).to_bytes(1, 'big')

        # Send BGP message over TCP socket
        while self.socket is None:
            time.sleep(0.1)
            
        self.socket.sendall(header + message)

    def preprocess_message(self):
        # Receive BGP message from TCP socket
    
        header = self.socket.recv(20)

        if not header:
            self.finished = True
            self.socket.close()
            return None, None, None
        
        message_length = int.from_bytes(header[16:18], 'big')
        message_type = int.from_bytes(header[18:19], 'big')
        need_response = int.from_bytes(header[19:20], 'big')
        message = self.socket.recv(message_length - 20)
        return message_type, message, need_response
        

    def open_session(self, need_response=True):
        # Construct OPEN message
        version = 4
        my_as = self.local_as
        hold_time = 180
        bgp_id = self.local_ip

        # Send OPEN message
        open_message = version.to_bytes(1, 'big') + my_as.to_bytes(2, 'big') + \
            hold_time.to_bytes(2, 'big') + bytes(map(int, bgp_id.split('.')))
        
        # Load own certificate and private key
        pkey = self.load_private_key()

        # Calculate the hash of the OPEN message
        digest = hashlib.sha256(open_message).digest()

        # Sign the hash using own private key
        signature = crypto.sign(pkey, digest, "sha256")

        self.send_message(OPEN, open_message + signature, need_response)

    def update_routes(self, routes, is_withdraw=False):
        # Construct UPDATE message
        # Assuming we are sending IPv4 prefixes
        withdrawn_routes = b''
        if is_withdraw:
            for route in routes:
                prefix = route.split('/')
                prefix_length = int(prefix[1])
                prefix_bytes = bytes(map(int, prefix[0].split('.')))
                withdrawn_routes += (prefix_length.to_bytes(1, 'big') + prefix_bytes)

        else:
            path_attributes = b''
            nlri = b''
            for route in routes:
                prefix = route.split('/')
                prefix_bytes = bytes(map(int, prefix[0].split('.')))
                nlri += (prefix_bytes + (int(prefix[1]) // 8).to_bytes(1, 'big'))

        # Send UPDATE message
        if is_withdraw:
            update_message = withdrawn_routes
        else:
            update_message = path_attributes + nlri

        while not self.connected:
            time.sleep(0.1)

        self.send_message(UPDATE, update_message)
   
    def close_session(self):
        # Send NOTIFICATION message
        error_code = 6
        error_subcode = 2
        data = b''
        notification_message = error_code.to_bytes(1, 'big') + \
            error_subcode.to_bytes(1, 'big') + data
        
        while not self.connected:
            time.sleep(0.1)

        self.send_message(NOTIFICATION, notification_message)

    def receive_message(self):
        
        while not self.finished:
            if self.socket is None:
                time.sleep(0.1)
                continue

            # receive message
            message_type, message, need_response = self.preprocess_message()
            
            if message_type == OPEN:
                print(f'Received OPEN message : {message}')

                if need_response:
                    # Send the response as an OPEN message
                    self.open_session(False)

                if self.verify_open_message(message):
                    self.connected = True
                
 
            elif message_type == UPDATE:
                print(f'Received UPDATE message: {message}')
                if need_response:
                    # Process the received UPDATE message and generate a response
                    response = b'This is the response to the UPDATE message'
                    # Send the response as an UPDATE message
                    self.send_message(UPDATE, response, False)

            elif message_type == NOTIFICATION:
                print(f'Received NOTIFICATION message: {message}')
                self.finished = True
                self.socket.close()

            else:
                pass
    