from bgp import BGPSpeaker
import threading

if __name__ == "__main__":
  
    # Create BGP speakers

    speaker = BGPSpeaker('127.0.0.2', 65001, '127.0.0.1', 65000, 2826, 2825)
    speaker.generate_certificate()
    
    # Start listening on separate thread
    listener = threading.Thread(target=speaker.listen)
    listener.start()
    receiver = threading.Thread(target=speaker.receive_message)
    receiver.start()

    # Simulate BGP message exchange
    routes = ['147.0.0.0/24', '148.168.0.0/16']
    speaker.update_routes(routes)

    routes = ['147.0.0.0/24']
    speaker.update_routes(routes, True)

    # Wait for the listeners to finish
    listener.join()
    receiver.join()