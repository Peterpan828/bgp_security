from bgp import BGPSpeaker
import threading
import time

if __name__ == "__main__":

    # Create BGP speakers
    speaker = BGPSpeaker('127.0.0.1', 65000, '127.0.0.2', 65001, 2825, 2826)
    speaker.generate_certificate()

    # Connect BGP speakers
    speaker.connect()

    receiver = threading.Thread(target=speaker.receive_message)
    receiver.start()
    # Open BGP session
    speaker.open_session()
    
    # Simulate BGP message exchange
    routes = ['10.0.0.0/24', '192.168.0.0/16']
    speaker.update_routes(routes)
    
    routes = ['192.168.0.0/16']
    speaker.update_routes(routes, True)

    # Close BGP sessions
    time.sleep(1)
    speaker.close_session()

    # Waiting for the receiver to finish
    receiver.join()